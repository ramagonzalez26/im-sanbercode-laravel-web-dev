
<?php 

require_once 'animal.php';
require_once 'Frog.php';
require_once 'Ape.php';


$sheep = new animal("shaun");

echo "nama :".$sheep->name."<br>"; // "shaun"
echo "legs :".$sheep->legs."<br>"; // 4
echo "cold blooded :".$sheep->cold_blooded."<br><br>"; // "no"


$sungokong = new Ape("kera sakti");
echo "nama :".$sungokong->name."<br>"; //
echo "legs :".$sungokong->legs."<br>"; // 2
echo "cold blooded :".$sungokong->cold_blooded."<br>"; // "no"
echo "yell :"; // "Auooo"
$sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");
echo "nama :".$kodok->name."<br>"; // "shaun"
echo "legs :".$kodok->legs."<br>"; // 4
echo "cold blooded :".$kodok->cold_blooded."<br>"; // "no"
echo "jump :"; // "hop hop"
$kodok->jump() ; // "hop hop"

?>